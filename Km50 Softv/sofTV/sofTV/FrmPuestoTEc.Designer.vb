﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPuestoTEc
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim ClaveLabel As System.Windows.Forms.Label
        Dim DescripcionLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmPuestoTEc))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.CONPUESTOTECBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.CONPUESTOTECBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton
        Me.CONPUESTOTECBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton
        Me.ClaveTextBox = New System.Windows.Forms.TextBox
        Me.DescripcionTextBox = New System.Windows.Forms.TextBox
        Me.Button5 = New System.Windows.Forms.Button
        Me.CONPUESTOTECTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONPUESTOTECTableAdapter
        ClaveLabel = New System.Windows.Forms.Label
        DescripcionLabel = New System.Windows.Forms.Label
        Me.Panel1.SuspendLayout()
        CType(Me.CONPUESTOTECBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONPUESTOTECBindingNavigator.SuspendLayout()
        CType(Me.CONPUESTOTECBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ClaveLabel
        '
        ClaveLabel.AutoSize = True
        ClaveLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ClaveLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ClaveLabel.Location = New System.Drawing.Point(107, 65)
        ClaveLabel.Name = "ClaveLabel"
        ClaveLabel.Size = New System.Drawing.Size(46, 15)
        ClaveLabel.TabIndex = 0
        ClaveLabel.Text = "Clave:"
        '
        'DescripcionLabel
        '
        DescripcionLabel.AutoSize = True
        DescripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescripcionLabel.ForeColor = System.Drawing.Color.LightSlateGray
        DescripcionLabel.Location = New System.Drawing.Point(66, 93)
        DescripcionLabel.Name = "DescripcionLabel"
        DescripcionLabel.Size = New System.Drawing.Size(87, 15)
        DescripcionLabel.TabIndex = 2
        DescripcionLabel.Text = "Descripcion:"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.CONPUESTOTECBindingNavigator)
        Me.Panel1.Controls.Add(ClaveLabel)
        Me.Panel1.Controls.Add(Me.ClaveTextBox)
        Me.Panel1.Controls.Add(DescripcionLabel)
        Me.Panel1.Controls.Add(Me.DescripcionTextBox)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(592, 195)
        Me.Panel1.TabIndex = 0
        '
        'CONPUESTOTECBindingNavigator
        '
        Me.CONPUESTOTECBindingNavigator.AddNewItem = Nothing
        Me.CONPUESTOTECBindingNavigator.BindingSource = Me.CONPUESTOTECBindingSource
        Me.CONPUESTOTECBindingNavigator.CountItem = Nothing
        Me.CONPUESTOTECBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONPUESTOTECBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.BindingNavigatorDeleteItem, Me.CONPUESTOTECBindingNavigatorSaveItem})
        Me.CONPUESTOTECBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONPUESTOTECBindingNavigator.MoveFirstItem = Nothing
        Me.CONPUESTOTECBindingNavigator.MoveLastItem = Nothing
        Me.CONPUESTOTECBindingNavigator.MoveNextItem = Nothing
        Me.CONPUESTOTECBindingNavigator.MovePreviousItem = Nothing
        Me.CONPUESTOTECBindingNavigator.Name = "CONPUESTOTECBindingNavigator"
        Me.CONPUESTOTECBindingNavigator.PositionItem = Nothing
        Me.CONPUESTOTECBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONPUESTOTECBindingNavigator.Size = New System.Drawing.Size(592, 25)
        Me.CONPUESTOTECBindingNavigator.TabIndex = 1
        Me.CONPUESTOTECBindingNavigator.TabStop = True
        Me.CONPUESTOTECBindingNavigator.Text = "BindingNavigator1"
        '
        'CONPUESTOTECBindingSource
        '
        Me.CONPUESTOTECBindingSource.DataMember = "CONPUESTOTEC"
        Me.CONPUESTOTECBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(90, 22)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(79, 22)
        Me.ToolStripButton1.Text = "&CANCELAR"
        '
        'CONPUESTOTECBindingNavigatorSaveItem
        '
        Me.CONPUESTOTECBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONPUESTOTECBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONPUESTOTECBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONPUESTOTECBindingNavigatorSaveItem.Name = "CONPUESTOTECBindingNavigatorSaveItem"
        Me.CONPUESTOTECBindingNavigatorSaveItem.Size = New System.Drawing.Size(91, 22)
        Me.CONPUESTOTECBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'ClaveTextBox
        '
        Me.ClaveTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ClaveTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONPUESTOTECBindingSource, "Clave", True))
        Me.ClaveTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClaveTextBox.Location = New System.Drawing.Point(159, 63)
        Me.ClaveTextBox.Name = "ClaveTextBox"
        Me.ClaveTextBox.ReadOnly = True
        Me.ClaveTextBox.Size = New System.Drawing.Size(100, 21)
        Me.ClaveTextBox.TabIndex = 10
        Me.ClaveTextBox.TabStop = False
        '
        'DescripcionTextBox
        '
        Me.DescripcionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DescripcionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONPUESTOTECBindingSource, "Descripcion", True))
        Me.DescripcionTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionTextBox.Location = New System.Drawing.Point(159, 91)
        Me.DescripcionTextBox.MaxLength = 80
        Me.DescripcionTextBox.Name = "DescripcionTextBox"
        Me.DescripcionTextBox.Size = New System.Drawing.Size(319, 21)
        Me.DescripcionTextBox.TabIndex = 0
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(468, 213)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 2
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'CONPUESTOTECTableAdapter
        '
        Me.CONPUESTOTECTableAdapter.ClearBeforeFill = True
        '
        'FrmPuestoTEc
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(616, 254)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "FrmPuestoTEc"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Puestos del Técnico"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.CONPUESTOTECBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONPUESTOTECBindingNavigator.ResumeLayout(False)
        Me.CONPUESTOTECBindingNavigator.PerformLayout()
        CType(Me.CONPUESTOTECBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONPUESTOTECBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONPUESTOTECTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONPUESTOTECTableAdapter
    Friend WithEvents CONPUESTOTECBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONPUESTOTECBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ClaveTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DescripcionTextBox As System.Windows.Forms.TextBox
End Class
