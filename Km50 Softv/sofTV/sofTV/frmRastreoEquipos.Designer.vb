﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRastreoEquipos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblClave = New System.Windows.Forms.Label()
        Me.lblSerieAparato = New System.Windows.Forms.Label()
        Me.lblEstadoAparato = New System.Windows.Forms.Label()
        Me.lblUbicacion = New System.Windows.Forms.Label()
        Me.lblDescripcion = New System.Windows.Forms.Label()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.gbxDatosAparato = New System.Windows.Forms.GroupBox()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.txtUbicacion = New System.Windows.Forms.TextBox()
        Me.txtEstadoAparato = New System.Windows.Forms.TextBox()
        Me.txtSerieAparato = New System.Windows.Forms.TextBox()
        Me.txtClave = New System.Windows.Forms.TextBox()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.gbxDatosAparato.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblClave
        '
        Me.lblClave.AutoSize = True
        Me.lblClave.Location = New System.Drawing.Point(74, 33)
        Me.lblClave.Name = "lblClave"
        Me.lblClave.Size = New System.Drawing.Size(56, 16)
        Me.lblClave.TabIndex = 0
        Me.lblClave.Text = "Clave :"
        '
        'lblSerieAparato
        '
        Me.lblSerieAparato.AutoSize = True
        Me.lblSerieAparato.Location = New System.Drawing.Point(18, 65)
        Me.lblSerieAparato.Name = "lblSerieAparato"
        Me.lblSerieAparato.Size = New System.Drawing.Size(112, 16)
        Me.lblSerieAparato.TabIndex = 1
        Me.lblSerieAparato.Text = "Serie Aparato :"
        '
        'lblEstadoAparato
        '
        Me.lblEstadoAparato.AutoSize = True
        Me.lblEstadoAparato.Location = New System.Drawing.Point(6, 96)
        Me.lblEstadoAparato.Name = "lblEstadoAparato"
        Me.lblEstadoAparato.Size = New System.Drawing.Size(124, 16)
        Me.lblEstadoAparato.TabIndex = 2
        Me.lblEstadoAparato.Text = "Estado Aparato :"
        '
        'lblUbicacion
        '
        Me.lblUbicacion.AutoSize = True
        Me.lblUbicacion.Location = New System.Drawing.Point(44, 128)
        Me.lblUbicacion.Name = "lblUbicacion"
        Me.lblUbicacion.Size = New System.Drawing.Size(86, 16)
        Me.lblUbicacion.TabIndex = 3
        Me.lblUbicacion.Text = "Ubicación :"
        '
        'lblDescripcion
        '
        Me.lblDescripcion.AutoSize = True
        Me.lblDescripcion.Location = New System.Drawing.Point(31, 159)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(99, 16)
        Me.lblDescripcion.TabIndex = 4
        Me.lblDescripcion.Text = "Descripción :"
        '
        'btnAceptar
        '
        Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.Location = New System.Drawing.Point(165, 214)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(128, 27)
        Me.btnAceptar.TabIndex = 5
        Me.btnAceptar.Text = "&Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'gbxDatosAparato
        '
        Me.gbxDatosAparato.Controls.Add(Me.txtDescripcion)
        Me.gbxDatosAparato.Controls.Add(Me.txtUbicacion)
        Me.gbxDatosAparato.Controls.Add(Me.txtEstadoAparato)
        Me.gbxDatosAparato.Controls.Add(Me.txtSerieAparato)
        Me.gbxDatosAparato.Controls.Add(Me.txtClave)
        Me.gbxDatosAparato.Controls.Add(Me.lblClave)
        Me.gbxDatosAparato.Controls.Add(Me.lblSerieAparato)
        Me.gbxDatosAparato.Controls.Add(Me.lblDescripcion)
        Me.gbxDatosAparato.Controls.Add(Me.lblEstadoAparato)
        Me.gbxDatosAparato.Controls.Add(Me.lblUbicacion)
        Me.gbxDatosAparato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxDatosAparato.Location = New System.Drawing.Point(12, 12)
        Me.gbxDatosAparato.Name = "gbxDatosAparato"
        Me.gbxDatosAparato.Size = New System.Drawing.Size(431, 196)
        Me.gbxDatosAparato.TabIndex = 6
        Me.gbxDatosAparato.TabStop = False
        Me.gbxDatosAparato.Text = "Detalle"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.Enabled = False
        Me.txtDescripcion.Location = New System.Drawing.Point(136, 156)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.ReadOnly = True
        Me.txtDescripcion.Size = New System.Drawing.Size(289, 22)
        Me.txtDescripcion.TabIndex = 12
        Me.txtDescripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtUbicacion
        '
        Me.txtUbicacion.Enabled = False
        Me.txtUbicacion.Location = New System.Drawing.Point(136, 125)
        Me.txtUbicacion.Name = "txtUbicacion"
        Me.txtUbicacion.ReadOnly = True
        Me.txtUbicacion.Size = New System.Drawing.Size(289, 22)
        Me.txtUbicacion.TabIndex = 11
        Me.txtUbicacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtEstadoAparato
        '
        Me.txtEstadoAparato.Enabled = False
        Me.txtEstadoAparato.Location = New System.Drawing.Point(136, 93)
        Me.txtEstadoAparato.Name = "txtEstadoAparato"
        Me.txtEstadoAparato.ReadOnly = True
        Me.txtEstadoAparato.Size = New System.Drawing.Size(289, 22)
        Me.txtEstadoAparato.TabIndex = 10
        Me.txtEstadoAparato.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtSerieAparato
        '
        Me.txtSerieAparato.Enabled = False
        Me.txtSerieAparato.Location = New System.Drawing.Point(136, 62)
        Me.txtSerieAparato.Name = "txtSerieAparato"
        Me.txtSerieAparato.ReadOnly = True
        Me.txtSerieAparato.Size = New System.Drawing.Size(289, 22)
        Me.txtSerieAparato.TabIndex = 6
        Me.txtSerieAparato.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtClave
        '
        Me.txtClave.Enabled = False
        Me.txtClave.Location = New System.Drawing.Point(136, 30)
        Me.txtClave.Name = "txtClave"
        Me.txtClave.ReadOnly = True
        Me.txtClave.Size = New System.Drawing.Size(289, 22)
        Me.txtClave.TabIndex = 5
        Me.txtClave.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'frmRastreoEquipos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(455, 254)
        Me.Controls.Add(Me.gbxDatosAparato)
        Me.Controls.Add(Me.btnAceptar)
        Me.Name = "frmRastreoEquipos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ubicación Aparato"
        Me.gbxDatosAparato.ResumeLayout(False)
        Me.gbxDatosAparato.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblClave As System.Windows.Forms.Label
    Friend WithEvents lblSerieAparato As System.Windows.Forms.Label
    Friend WithEvents lblEstadoAparato As System.Windows.Forms.Label
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents gbxDatosAparato As System.Windows.Forms.GroupBox
    Friend WithEvents lblUbicacion As System.Windows.Forms.Label
    Public WithEvents txtClave As System.Windows.Forms.TextBox
    Public WithEvents txtSerieAparato As System.Windows.Forms.TextBox
    Public WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Public WithEvents txtUbicacion As System.Windows.Forms.TextBox
    Public WithEvents txtEstadoAparato As System.Windows.Forms.TextBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class
