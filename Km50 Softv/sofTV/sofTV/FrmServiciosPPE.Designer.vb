<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmServiciosPPE
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim HitsLabel As System.Windows.Forms.Label
        Dim ClasificacionLabel As System.Windows.Forms.Label
        Dim PrecioLabel As System.Windows.Forms.Label
        Dim DescripcionLabel As System.Windows.Forms.Label
        Dim Clv_TxtLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.lblClasificacion = New System.Windows.Forms.Label
        Me.gvMuestraProgramacion = New System.Windows.Forms.DataGridView
        Me.lblHits = New System.Windows.Forms.Label
        Me.btnAceptar = New System.Windows.Forms.Button
        Me.lblPrecio = New System.Windows.Forms.Label
        Me.lblDescripcion = New System.Windows.Forms.Label
        Me.lblClv = New System.Windows.Forms.Label
        Me.dtmFecha = New System.Windows.Forms.DateTimePicker
        Me.btnBusca = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.btnSalir = New System.Windows.Forms.Button
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.gbDetalleServicio = New System.Windows.Forms.GroupBox
        Me.gbBuscaServicio = New System.Windows.Forms.GroupBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtDescripcion = New System.Windows.Forms.TextBox
        Me.gbBuscaContratos = New System.Windows.Forms.GroupBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblPuntos = New System.Windows.Forms.Label
        Me.btnBuscaContrato = New System.Windows.Forms.Button
        Me.LblStatus = New System.Windows.Forms.Label
        Me.lblPuntosNecesarios = New System.Windows.Forms.Label
        Me.txtBuscaContrato = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.gvMuestraServicios = New System.Windows.Forms.DataGridView
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.gvPelis = New System.Windows.Forms.DataGridView
        Me.Clv_Servicio = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Clv_Txt = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Precio = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Clv_PPE = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Hits = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Clasificacion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Clv_Progra = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Clv_Txt1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Fecha = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.btnQuitar = New System.Windows.Forms.Button
        Me.btnAgregar = New System.Windows.Forms.Button
        Me.btnLimpiar = New System.Windows.Forms.Button
        HitsLabel = New System.Windows.Forms.Label
        ClasificacionLabel = New System.Windows.Forms.Label
        PrecioLabel = New System.Windows.Forms.Label
        DescripcionLabel = New System.Windows.Forms.Label
        Clv_TxtLabel = New System.Windows.Forms.Label
        CType(Me.gvMuestraProgramacion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.gbDetalleServicio.SuspendLayout()
        Me.gbBuscaServicio.SuspendLayout()
        Me.gbBuscaContratos.SuspendLayout()
        CType(Me.gvMuestraServicios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gvPelis, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'HitsLabel
        '
        HitsLabel.AutoSize = True
        HitsLabel.BackColor = System.Drawing.Color.WhiteSmoke
        HitsLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        HitsLabel.ForeColor = System.Drawing.Color.Black
        HitsLabel.Location = New System.Drawing.Point(8, 163)
        HitsLabel.Name = "HitsLabel"
        HitsLabel.Size = New System.Drawing.Size(39, 16)
        HitsLabel.TabIndex = 21
        HitsLabel.Text = "Hits:"
        '
        'ClasificacionLabel
        '
        ClasificacionLabel.AutoSize = True
        ClasificacionLabel.BackColor = System.Drawing.Color.WhiteSmoke
        ClasificacionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ClasificacionLabel.ForeColor = System.Drawing.Color.Black
        ClasificacionLabel.Location = New System.Drawing.Point(8, 199)
        ClasificacionLabel.Name = "ClasificacionLabel"
        ClasificacionLabel.Size = New System.Drawing.Size(105, 16)
        ClasificacionLabel.TabIndex = 22
        ClasificacionLabel.Text = "Clasificación :"
        '
        'PrecioLabel
        '
        PrecioLabel.AutoSize = True
        PrecioLabel.BackColor = System.Drawing.Color.WhiteSmoke
        PrecioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PrecioLabel.ForeColor = System.Drawing.Color.Black
        PrecioLabel.Location = New System.Drawing.Point(8, 122)
        PrecioLabel.Name = "PrecioLabel"
        PrecioLabel.Size = New System.Drawing.Size(57, 16)
        PrecioLabel.TabIndex = 20
        PrecioLabel.Text = "Precio:"
        '
        'DescripcionLabel
        '
        DescripcionLabel.AutoSize = True
        DescripcionLabel.BackColor = System.Drawing.Color.WhiteSmoke
        DescripcionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescripcionLabel.ForeColor = System.Drawing.Color.Black
        DescripcionLabel.Location = New System.Drawing.Point(8, 66)
        DescripcionLabel.Name = "DescripcionLabel"
        DescripcionLabel.Size = New System.Drawing.Size(99, 16)
        DescripcionLabel.TabIndex = 19
        DescripcionLabel.Text = "Descripción :"
        '
        'Clv_TxtLabel
        '
        Clv_TxtLabel.AutoSize = True
        Clv_TxtLabel.BackColor = System.Drawing.Color.WhiteSmoke
        Clv_TxtLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_TxtLabel.ForeColor = System.Drawing.Color.Black
        Clv_TxtLabel.Location = New System.Drawing.Point(8, 25)
        Clv_TxtLabel.Name = "Clv_TxtLabel"
        Clv_TxtLabel.Size = New System.Drawing.Size(56, 16)
        Clv_TxtLabel.TabIndex = 18
        Clv_TxtLabel.Text = "Clave :"
        '
        'lblClasificacion
        '
        Me.lblClasificacion.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblClasificacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClasificacion.Location = New System.Drawing.Point(11, 215)
        Me.lblClasificacion.Name = "lblClasificacion"
        Me.lblClasificacion.Size = New System.Drawing.Size(226, 15)
        Me.lblClasificacion.TabIndex = 23
        '
        'gvMuestraProgramacion
        '
        Me.gvMuestraProgramacion.AllowUserToAddRows = False
        Me.gvMuestraProgramacion.AllowUserToDeleteRows = False
        Me.gvMuestraProgramacion.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gvMuestraProgramacion.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.gvMuestraProgramacion.ColumnHeadersHeight = 25
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.gvMuestraProgramacion.DefaultCellStyle = DataGridViewCellStyle8
        Me.gvMuestraProgramacion.Location = New System.Drawing.Point(280, 424)
        Me.gvMuestraProgramacion.MultiSelect = False
        Me.gvMuestraProgramacion.Name = "gvMuestraProgramacion"
        Me.gvMuestraProgramacion.ReadOnly = True
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gvMuestraProgramacion.RowHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.gvMuestraProgramacion.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.gvMuestraProgramacion.Size = New System.Drawing.Size(429, 305)
        Me.gvMuestraProgramacion.TabIndex = 22
        Me.gvMuestraProgramacion.TabStop = False
        '
        'lblHits
        '
        Me.lblHits.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblHits.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHits.Location = New System.Drawing.Point(11, 179)
        Me.lblHits.Name = "lblHits"
        Me.lblHits.Size = New System.Drawing.Size(226, 15)
        Me.lblHits.TabIndex = 22
        '
        'btnAceptar
        '
        Me.btnAceptar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.Location = New System.Drawing.Point(811, 603)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(136, 36)
        Me.btnAceptar.TabIndex = 26
        Me.btnAceptar.Text = "&ACEPTAR"
        Me.btnAceptar.UseVisualStyleBackColor = False
        '
        'lblPrecio
        '
        Me.lblPrecio.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblPrecio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrecio.Location = New System.Drawing.Point(11, 138)
        Me.lblPrecio.Name = "lblPrecio"
        Me.lblPrecio.Size = New System.Drawing.Size(226, 15)
        Me.lblPrecio.TabIndex = 21
        '
        'lblDescripcion
        '
        Me.lblDescripcion.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescripcion.Location = New System.Drawing.Point(11, 82)
        Me.lblDescripcion.Name = "lblDescripcion"
        Me.lblDescripcion.Size = New System.Drawing.Size(226, 33)
        Me.lblDescripcion.TabIndex = 20
        '
        'lblClv
        '
        Me.lblClv.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblClv.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClv.Location = New System.Drawing.Point(11, 41)
        Me.lblClv.Name = "lblClv"
        Me.lblClv.Size = New System.Drawing.Size(226, 15)
        Me.lblClv.TabIndex = 19
        '
        'dtmFecha
        '
        Me.dtmFecha.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtmFecha.CustomFormat = "dd-mm-aaaa"
        Me.dtmFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtmFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtmFecha.Location = New System.Drawing.Point(6, 92)
        Me.dtmFecha.Name = "dtmFecha"
        Me.dtmFecha.Size = New System.Drawing.Size(229, 22)
        Me.dtmFecha.TabIndex = 26
        Me.dtmFecha.Value = New Date(2008, 1, 18, 0, 0, 0, 0)
        '
        'btnBusca
        '
        Me.btnBusca.BackColor = System.Drawing.Color.DarkOrange
        Me.btnBusca.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBusca.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBusca.Location = New System.Drawing.Point(65, 133)
        Me.btnBusca.Name = "btnBusca"
        Me.btnBusca.Size = New System.Drawing.Size(88, 23)
        Me.btnBusca.TabIndex = 7
        Me.btnBusca.Text = "Buscar"
        Me.btnBusca.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 72)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 16)
        Me.Label1.TabIndex = 24
        Me.Label1.Text = "Fecha :"
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.DarkOrange
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(811, 687)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(136, 36)
        Me.btnSalir.TabIndex = 27
        Me.btnSalir.Text = "&SALIR"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.gbDetalleServicio)
        Me.Panel1.Controls.Add(Me.gbBuscaServicio)
        Me.Panel1.Controls.Add(Me.gbBuscaContratos)
        Me.Panel1.Location = New System.Drawing.Point(3, 2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(271, 730)
        Me.Panel1.TabIndex = 25
        Me.Panel1.TabStop = True
        '
        'gbDetalleServicio
        '
        Me.gbDetalleServicio.Controls.Add(ClasificacionLabel)
        Me.gbDetalleServicio.Controls.Add(Clv_TxtLabel)
        Me.gbDetalleServicio.Controls.Add(Me.lblClasificacion)
        Me.gbDetalleServicio.Controls.Add(Me.lblClv)
        Me.gbDetalleServicio.Controls.Add(DescripcionLabel)
        Me.gbDetalleServicio.Controls.Add(PrecioLabel)
        Me.gbDetalleServicio.Controls.Add(Me.lblDescripcion)
        Me.gbDetalleServicio.Controls.Add(HitsLabel)
        Me.gbDetalleServicio.Controls.Add(Me.lblHits)
        Me.gbDetalleServicio.Controls.Add(Me.lblPrecio)
        Me.gbDetalleServicio.Location = New System.Drawing.Point(9, 465)
        Me.gbDetalleServicio.Name = "gbDetalleServicio"
        Me.gbDetalleServicio.Size = New System.Drawing.Size(244, 262)
        Me.gbDetalleServicio.TabIndex = 29
        Me.gbDetalleServicio.TabStop = False
        Me.gbDetalleServicio.Text = "Detalle del Servicio"
        '
        'gbBuscaServicio
        '
        Me.gbBuscaServicio.Controls.Add(Me.dtmFecha)
        Me.gbBuscaServicio.Controls.Add(Me.Label3)
        Me.gbBuscaServicio.Controls.Add(Me.btnBusca)
        Me.gbBuscaServicio.Controls.Add(Me.txtDescripcion)
        Me.gbBuscaServicio.Controls.Add(Me.Label1)
        Me.gbBuscaServicio.Location = New System.Drawing.Point(9, 290)
        Me.gbBuscaServicio.Name = "gbBuscaServicio"
        Me.gbBuscaServicio.Size = New System.Drawing.Size(244, 169)
        Me.gbBuscaServicio.TabIndex = 28
        Me.gbBuscaServicio.TabStop = False
        Me.gbBuscaServicio.Text = "Buscar Servicio PPE:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(6, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(99, 16)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "Descripción :"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescripcion.Location = New System.Drawing.Point(4, 35)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(228, 22)
        Me.txtDescripcion.TabIndex = 2
        '
        'gbBuscaContratos
        '
        Me.gbBuscaContratos.Controls.Add(Me.Label4)
        Me.gbBuscaContratos.Controls.Add(Me.Label2)
        Me.gbBuscaContratos.Controls.Add(Me.lblPuntos)
        Me.gbBuscaContratos.Controls.Add(Me.btnBuscaContrato)
        Me.gbBuscaContratos.Controls.Add(Me.LblStatus)
        Me.gbBuscaContratos.Controls.Add(Me.lblPuntosNecesarios)
        Me.gbBuscaContratos.Controls.Add(Me.txtBuscaContrato)
        Me.gbBuscaContratos.Controls.Add(Me.Label6)
        Me.gbBuscaContratos.Location = New System.Drawing.Point(9, 11)
        Me.gbBuscaContratos.Name = "gbBuscaContratos"
        Me.gbBuscaContratos.Size = New System.Drawing.Size(244, 273)
        Me.gbBuscaContratos.TabIndex = 27
        Me.gbBuscaContratos.TabStop = False
        Me.gbBuscaContratos.Text = "Buscar Contratos"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(8, 228)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(137, 16)
        Me.Label4.TabIndex = 31
        Me.Label4.Text = "Puntos del Cliente:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(8, 129)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(145, 16)
        Me.Label2.TabIndex = 30
        Me.Label2.Text = "Nombre del Cliente:"
        '
        'lblPuntos
        '
        Me.lblPuntos.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblPuntos.AutoEllipsis = True
        Me.lblPuntos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPuntos.Location = New System.Drawing.Point(8, 244)
        Me.lblPuntos.Name = "lblPuntos"
        Me.lblPuntos.Size = New System.Drawing.Size(232, 26)
        Me.lblPuntos.TabIndex = 29
        '
        'btnBuscaContrato
        '
        Me.btnBuscaContrato.BackColor = System.Drawing.Color.DarkOrange
        Me.btnBuscaContrato.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuscaContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscaContrato.Location = New System.Drawing.Point(138, 47)
        Me.btnBuscaContrato.Name = "btnBuscaContrato"
        Me.btnBuscaContrato.Size = New System.Drawing.Size(31, 23)
        Me.btnBuscaContrato.TabIndex = 28
        Me.btnBuscaContrato.Text = "..."
        Me.btnBuscaContrato.UseVisualStyleBackColor = False
        '
        'LblStatus
        '
        Me.LblStatus.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblStatus.AutoEllipsis = True
        Me.LblStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblStatus.Location = New System.Drawing.Point(7, 145)
        Me.LblStatus.Name = "LblStatus"
        Me.LblStatus.Size = New System.Drawing.Size(230, 74)
        Me.LblStatus.TabIndex = 20
        '
        'lblPuntosNecesarios
        '
        Me.lblPuntosNecesarios.AutoSize = True
        Me.lblPuntosNecesarios.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPuntosNecesarios.Location = New System.Drawing.Point(8, 94)
        Me.lblPuntosNecesarios.Name = "lblPuntosNecesarios"
        Me.lblPuntosNecesarios.Size = New System.Drawing.Size(147, 16)
        Me.lblPuntosNecesarios.TabIndex = 19
        Me.lblPuntosNecesarios.Text = "Puntos Necesarios: "
        '
        'txtBuscaContrato
        '
        Me.txtBuscaContrato.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBuscaContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBuscaContrato.Location = New System.Drawing.Point(6, 48)
        Me.txtBuscaContrato.Name = "txtBuscaContrato"
        Me.txtBuscaContrato.Size = New System.Drawing.Size(126, 22)
        Me.txtBuscaContrato.TabIndex = 17
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(11, 29)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(74, 16)
        Me.Label6.TabIndex = 16
        Me.Label6.Text = "Contrato: "
        '
        'gvMuestraServicios
        '
        Me.gvMuestraServicios.AllowUserToAddRows = False
        Me.gvMuestraServicios.AllowUserToDeleteRows = False
        Me.gvMuestraServicios.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gvMuestraServicios.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle10
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.gvMuestraServicios.DefaultCellStyle = DataGridViewCellStyle11
        Me.gvMuestraServicios.Location = New System.Drawing.Point(283, 23)
        Me.gvMuestraServicios.MultiSelect = False
        Me.gvMuestraServicios.Name = "gvMuestraServicios"
        Me.gvMuestraServicios.ReadOnly = True
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.gvMuestraServicios.RowHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.gvMuestraServicios.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.gvMuestraServicios.Size = New System.Drawing.Size(429, 377)
        Me.gvMuestraServicios.TabIndex = 29
        Me.gvMuestraServicios.TabStop = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(280, 2)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(176, 18)
        Me.Label5.TabIndex = 29
        Me.Label5.Text = "Servicios Disponibles:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(280, 403)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(172, 18)
        Me.Label7.TabIndex = 31
        Me.Label7.Text = "Horarios Disponibles:"
        '
        'gvPelis
        '
        Me.gvPelis.AllowUserToAddRows = False
        Me.gvPelis.AllowUserToDeleteRows = False
        Me.gvPelis.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.gvPelis.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gvPelis.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clv_Servicio, Me.Clv_Txt, Me.Descripcion, Me.Precio, Me.Clv_PPE, Me.Hits, Me.Clasificacion, Me.Clv_Progra, Me.Clv_Txt1, Me.Fecha})
        Me.gvPelis.Location = New System.Drawing.Point(4, 21)
        Me.gvPelis.MultiSelect = False
        Me.gvPelis.Name = "gvPelis"
        Me.gvPelis.ReadOnly = True
        Me.gvPelis.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.gvPelis.Size = New System.Drawing.Size(285, 312)
        Me.gvPelis.TabIndex = 32
        '
        'Clv_Servicio
        '
        Me.Clv_Servicio.HeaderText = "Clv_Servicio"
        Me.Clv_Servicio.Name = "Clv_Servicio"
        Me.Clv_Servicio.ReadOnly = True
        Me.Clv_Servicio.Visible = False
        '
        'Clv_Txt
        '
        Me.Clv_Txt.HeaderText = "Clv_Txt"
        Me.Clv_Txt.Name = "Clv_Txt"
        Me.Clv_Txt.ReadOnly = True
        Me.Clv_Txt.Visible = False
        '
        'Descripcion
        '
        Me.Descripcion.HeaderText = "Descripcion"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        '
        'Precio
        '
        Me.Precio.HeaderText = "Precio"
        Me.Precio.Name = "Precio"
        Me.Precio.ReadOnly = True
        Me.Precio.Visible = False
        '
        'Clv_PPE
        '
        Me.Clv_PPE.HeaderText = "Clv_PPE"
        Me.Clv_PPE.Name = "Clv_PPE"
        Me.Clv_PPE.ReadOnly = True
        Me.Clv_PPE.Visible = False
        '
        'Hits
        '
        Me.Hits.HeaderText = "Hits"
        Me.Hits.Name = "Hits"
        Me.Hits.ReadOnly = True
        Me.Hits.Visible = False
        '
        'Clasificacion
        '
        Me.Clasificacion.HeaderText = "Clasificacion"
        Me.Clasificacion.Name = "Clasificacion"
        Me.Clasificacion.ReadOnly = True
        Me.Clasificacion.Visible = False
        '
        'Clv_Progra
        '
        Me.Clv_Progra.HeaderText = "Clv_Progra"
        Me.Clv_Progra.Name = "Clv_Progra"
        Me.Clv_Progra.ReadOnly = True
        Me.Clv_Progra.Visible = False
        '
        'Clv_Txt1
        '
        Me.Clv_Txt1.HeaderText = "Clv_Txt1"
        Me.Clv_Txt1.Name = "Clv_Txt1"
        Me.Clv_Txt1.ReadOnly = True
        Me.Clv_Txt1.Visible = False
        '
        'Fecha
        '
        Me.Fecha.HeaderText = "Fecha"
        Me.Fecha.Name = "Fecha"
        Me.Fecha.ReadOnly = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnQuitar)
        Me.GroupBox1.Controls.Add(Me.gvPelis)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(718, 23)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(295, 377)
        Me.GroupBox1.TabIndex = 34
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Peliculas Agregadas"
        '
        'btnQuitar
        '
        Me.btnQuitar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnQuitar.Enabled = False
        Me.btnQuitar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnQuitar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnQuitar.Location = New System.Drawing.Point(106, 339)
        Me.btnQuitar.Name = "btnQuitar"
        Me.btnQuitar.Size = New System.Drawing.Size(88, 32)
        Me.btnQuitar.TabIndex = 35
        Me.btnQuitar.Text = "Quitar"
        Me.btnQuitar.UseVisualStyleBackColor = False
        Me.btnQuitar.Visible = False
        '
        'btnAgregar
        '
        Me.btnAgregar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnAgregar.Enabled = False
        Me.btnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregar.Location = New System.Drawing.Point(722, 453)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(88, 32)
        Me.btnAgregar.TabIndex = 27
        Me.btnAgregar.Text = "Agregar"
        Me.btnAgregar.UseVisualStyleBackColor = False
        Me.btnAgregar.Visible = False
        '
        'btnLimpiar
        '
        Me.btnLimpiar.BackColor = System.Drawing.Color.DarkOrange
        Me.btnLimpiar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLimpiar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLimpiar.Location = New System.Drawing.Point(811, 645)
        Me.btnLimpiar.Name = "btnLimpiar"
        Me.btnLimpiar.Size = New System.Drawing.Size(136, 36)
        Me.btnLimpiar.TabIndex = 35
        Me.btnLimpiar.Text = "&LIMPIAR"
        Me.btnLimpiar.UseVisualStyleBackColor = False
        Me.btnLimpiar.Visible = False
        '
        'FrmServiciosPPE
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1016, 734)
        Me.Controls.Add(Me.btnLimpiar)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.gvMuestraServicios)
        Me.Controls.Add(Me.gvMuestraProgramacion)
        Me.MaximizeBox = False
        Me.Name = "FrmServiciosPPE"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Servicios Pago Por Evento"
        CType(Me.gvMuestraProgramacion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.gbDetalleServicio.ResumeLayout(False)
        Me.gbDetalleServicio.PerformLayout()
        Me.gbBuscaServicio.ResumeLayout(False)
        Me.gbBuscaServicio.PerformLayout()
        Me.gbBuscaContratos.ResumeLayout(False)
        Me.gbBuscaContratos.PerformLayout()
        CType(Me.gvMuestraServicios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gvPelis, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblClasificacion As System.Windows.Forms.Label
    Friend WithEvents gvMuestraProgramacion As System.Windows.Forms.DataGridView
    Friend WithEvents lblHits As System.Windows.Forms.Label
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents lblPrecio As System.Windows.Forms.Label
    Friend WithEvents lblDescripcion As System.Windows.Forms.Label
    Friend WithEvents lblClv As System.Windows.Forms.Label
    Friend WithEvents dtmFecha As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnBusca As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents gvMuestraServicios As System.Windows.Forms.DataGridView
    Friend WithEvents gbBuscaContratos As System.Windows.Forms.GroupBox
    Friend WithEvents txtBuscaContrato As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lblPuntosNecesarios As System.Windows.Forms.Label
    Friend WithEvents btnBuscaContrato As System.Windows.Forms.Button
    Friend WithEvents LblStatus As System.Windows.Forms.Label
    Friend WithEvents gbBuscaServicio As System.Windows.Forms.GroupBox
    Friend WithEvents gbDetalleServicio As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents gvPelis As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents lblPuntos As System.Windows.Forms.Label
    Friend WithEvents btnQuitar As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Clv_Servicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Txt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Precio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_PPE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Hits As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clasificacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Progra As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clv_Txt1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnLimpiar As System.Windows.Forms.Button
End Class
