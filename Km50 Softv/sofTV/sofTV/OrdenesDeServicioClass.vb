﻿Imports System.Data.SqlClient
Imports System.Collections.Generic

Public Class OrdenesDeServicioClass
    Public Diccionario As Dictionary(Of String, Object)

    Public Sub uspBorraQuejasOrdenes(ByVal clvOrdenQueja As Long, ByVal tipo As String)
        BaseII.limpiaParametros()
        Diccionario = New Dictionary(Of String, Object)

        BaseII.CreateMyParameter("@clvOrdenQueja", SqlDbType.BigInt, clvOrdenQueja)
        BaseII.CreateMyParameter("@tipo", SqlDbType.VarChar, tipo, 1)
        BaseII.CreateMyParameter("@bndOrdenQueja", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.CreateMyParameter("@bndDescarga", ParameterDirection.Output, SqlDbType.Bit)

        Diccionario = BaseII.ProcedimientoOutPut("uspBorraQuejasOrdenes")
    End Sub
End Class
