<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ImpresionEstadosDeCuenta
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CMBBtnBusca1 = New System.Windows.Forms.Button
        Me.CMBBtnBusca2 = New System.Windows.Forms.Button
        Me.TxtContratoFin = New System.Windows.Forms.TextBox
        Me.TxtContratoIni = New System.Windows.Forms.TextBox
        Me.CMBBtnImprimir = New System.Windows.Forms.Button
        Me.CMBBtnSalir = New System.Windows.Forms.Button
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.CMBLblContratoFin = New System.Windows.Forms.Label
        Me.CMBLblContratoIni = New System.Windows.Forms.Label
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'CMBBtnBusca1
        '
        Me.CMBBtnBusca1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBBtnBusca1.Location = New System.Drawing.Point(253, 16)
        Me.CMBBtnBusca1.Name = "CMBBtnBusca1"
        Me.CMBBtnBusca1.Size = New System.Drawing.Size(30, 20)
        Me.CMBBtnBusca1.TabIndex = 0
        Me.CMBBtnBusca1.Text = "..."
        Me.CMBBtnBusca1.UseVisualStyleBackColor = True
        '
        'CMBBtnBusca2
        '
        Me.CMBBtnBusca2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.CMBBtnBusca2.Location = New System.Drawing.Point(253, 61)
        Me.CMBBtnBusca2.Name = "CMBBtnBusca2"
        Me.CMBBtnBusca2.Size = New System.Drawing.Size(30, 23)
        Me.CMBBtnBusca2.TabIndex = 1
        Me.CMBBtnBusca2.Text = "..."
        Me.CMBBtnBusca2.UseVisualStyleBackColor = True
        '
        'TxtContratoFin
        '
        Me.TxtContratoFin.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtContratoFin.Location = New System.Drawing.Point(130, 62)
        Me.TxtContratoFin.MaxLength = 6
        Me.TxtContratoFin.Name = "TxtContratoFin"
        Me.TxtContratoFin.Size = New System.Drawing.Size(117, 21)
        Me.TxtContratoFin.TabIndex = 2
        '
        'TxtContratoIni
        '
        Me.TxtContratoIni.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtContratoIni.Location = New System.Drawing.Point(130, 17)
        Me.TxtContratoIni.MaxLength = 6
        Me.TxtContratoIni.Name = "TxtContratoIni"
        Me.TxtContratoIni.Size = New System.Drawing.Size(117, 21)
        Me.TxtContratoIni.TabIndex = 3
        '
        'CMBBtnImprimir
        '
        Me.CMBBtnImprimir.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.CMBBtnImprimir.Location = New System.Drawing.Point(75, 123)
        Me.CMBBtnImprimir.Name = "CMBBtnImprimir"
        Me.CMBBtnImprimir.Size = New System.Drawing.Size(108, 36)
        Me.CMBBtnImprimir.TabIndex = 4
        Me.CMBBtnImprimir.Text = "&Comenzar Impresión"
        Me.CMBBtnImprimir.UseVisualStyleBackColor = True
        '
        'CMBBtnSalir
        '
        Me.CMBBtnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.CMBBtnSalir.Location = New System.Drawing.Point(215, 123)
        Me.CMBBtnSalir.Name = "CMBBtnSalir"
        Me.CMBBtnSalir.Size = New System.Drawing.Size(109, 36)
        Me.CMBBtnSalir.TabIndex = 5
        Me.CMBBtnSalir.Text = "&SALIR"
        Me.CMBBtnSalir.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.CMBLblContratoFin)
        Me.Panel1.Controls.Add(Me.CMBLblContratoIni)
        Me.Panel1.Controls.Add(Me.TxtContratoIni)
        Me.Panel1.Controls.Add(Me.TxtContratoFin)
        Me.Panel1.Controls.Add(Me.CMBBtnBusca2)
        Me.Panel1.Controls.Add(Me.CMBBtnBusca1)
        Me.Panel1.Location = New System.Drawing.Point(7, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(317, 93)
        Me.Panel1.TabIndex = 6
        '
        'CMBLblContratoFin
        '
        Me.CMBLblContratoFin.AutoSize = True
        Me.CMBLblContratoFin.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLblContratoFin.Location = New System.Drawing.Point(18, 63)
        Me.CMBLblContratoFin.Name = "CMBLblContratoFin"
        Me.CMBLblContratoFin.Size = New System.Drawing.Size(104, 16)
        Me.CMBLblContratoFin.TabIndex = 5
        Me.CMBLblContratoFin.Text = "Contrato Final"
        '
        'CMBLblContratoIni
        '
        Me.CMBLblContratoIni.AutoSize = True
        Me.CMBLblContratoIni.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLblContratoIni.Location = New System.Drawing.Point(13, 18)
        Me.CMBLblContratoIni.Name = "CMBLblContratoIni"
        Me.CMBLblContratoIni.Size = New System.Drawing.Size(111, 16)
        Me.CMBLblContratoIni.TabIndex = 4
        Me.CMBLblContratoIni.Text = "Contrato Inicial"
        '
        'BackgroundWorker1
        '
        '
        'ImpresionEstadosDeCuenta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(344, 175)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.CMBBtnSalir)
        Me.Controls.Add(Me.CMBBtnImprimir)
        Me.Name = "ImpresionEstadosDeCuenta"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Impresión Estados De Cuenta Telefonía"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CMBBtnBusca1 As System.Windows.Forms.Button
    Friend WithEvents CMBBtnBusca2 As System.Windows.Forms.Button
    Friend WithEvents TxtContratoFin As System.Windows.Forms.TextBox
    Friend WithEvents TxtContratoIni As System.Windows.Forms.TextBox
    Friend WithEvents CMBBtnImprimir As System.Windows.Forms.Button
    Friend WithEvents CMBBtnSalir As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents CMBLblContratoFin As System.Windows.Forms.Label
    Friend WithEvents CMBLblContratoIni As System.Windows.Forms.Label
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
End Class
