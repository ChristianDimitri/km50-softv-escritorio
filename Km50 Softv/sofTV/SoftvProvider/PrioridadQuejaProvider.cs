using System;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting;
using Softv.Entities;
using SoftvConfiguration;

namespace Softv.Providers
{
  /// <summary>
  /// Class                   : Softv.Providers.PrioridadQuejaProvider
  /// Generated by            : Desarroll@, Class Generator (c) 2009
  /// Description             : PrioridadQueja Provider
  /// File                    : PrioridadQuejaProvider.cs
  /// Creation date           : 23/06/2011
  /// Creation time           : 06:05:58 p.m.
  /// </summary>
  public abstract class PrioridadQuejaProvider : Globals.DataAccess
  {
      /// <summary>
      /// La instancia del PrioridadQueja de servicios de bd
      /// </summary>
      private static PrioridadQuejaProvider _Instance = null;

      private static ObjectHandle obj;
      /// <summary>
      /// Generates a PrioridadQueja instance
      /// </summary>
      public static PrioridadQuejaProvider Instance
      {
          get
          {
              if (_Instance == null)
              {
                obj = Activator.CreateInstance(
                   SoftvSettings.Settings.PrioridadQueja.Assembly,
                   SoftvSettings.Settings.PrioridadQueja.DataClass);
              _Instance = (PrioridadQuejaProvider)obj.Unwrap();
              }
              return _Instance;
          }
      }

      /// <summary>
      /// Provider's default constructor
      /// </summary>
      public PrioridadQuejaProvider()
      {
      }
      /// <summary>
      /// Abstract method to add PrioridadQueja
      /// </summary>
      /// <param name="PrioridadQueja"></param>
      /// <returns></returns>
      public abstract int AddPrioridadQueja(PrioridadQuejaEntity entity_PrioridadQueja);

      /// <summary>
      /// Abstract method to delete PrioridadQueja
      /// </summary>
      public abstract int DeletePrioridadQueja(int? clvPrioridadQueja);

      /// <summary>
      /// Abstract method to update PrioridadQueja
      /// </summary>
      public abstract int EditPrioridadQueja(PrioridadQuejaEntity entity_PrioridadQueja);

      /// <summary>
      /// Abstract method to get all PrioridadQueja
      /// </summary>
      public abstract List<PrioridadQuejaEntity> GetPrioridadQueja();

      /// <summary>
      /// Abstract method to get by id
      /// </summary>
      public abstract PrioridadQuejaEntity GetPrioridadQuejaById(int clvPrioridadQueja);

      /// <summary>
      /// Converts data from reader to entity
      /// </summary>
      protected virtual PrioridadQuejaEntity GetPrioridadQuejaFromReader(IDataReader reader)
      {
          PrioridadQuejaEntity entity_PrioridadQueja = null;
          try
          {
              entity_PrioridadQueja = new PrioridadQuejaEntity();
              entity_PrioridadQueja.clvPrioridadQueja = reader["clvPrioridadQueja"] ==  DBNull.Value ? null : ((int?)(reader["clvPrioridadQueja"]));
              entity_PrioridadQueja.Descripcion = reader["Descripcion"].ToString() == "" || reader["Descripcion"] ==  DBNull.Value ? null : ((String)(reader["Descripcion"]));
          }
          catch (Exception ex)
          {
              throw new Exception("Error converting data to entity ", ex);
          }
          return entity_PrioridadQueja;
      }
      #region Customs Methods
      /// <summary>
      ///
      /// </summary>
      public abstract List<PrioridadQuejaEntity> GetPrioridadQuejaByclvPrioridadQueja(int clvPrioridadQueja);
      /// <summary>
      ///
      /// </summary>
      public abstract List<PrioridadQuejaEntity> GetPrioridadQuejaByDescripcion(String Descripcion);
      #endregion
  }
}
